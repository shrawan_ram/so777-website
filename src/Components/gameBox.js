import React, { Component } from "react";

export default class gameBox extends Component{
    state = {
        addClass: false
    }
    toggle() {
        this.setState({addClass: !this.state.addClass});
      }
    render(){
        let boxClass = ["header-right"];
        let login = ["login"];
        if(this.state.addClass) {
        login.push('right-bar-enabled');
        }
        let { image } = this.props.data;
        return(
            <div data-v-8f01e1aa="" class="casino-banner-item login-hover" onClick={this.toggle.bind(this)}>
                <img data-v-8f01e1aa="" class="img-fluid" data-src={image} src={image} lazy="loaded" />
                <div data-v-8f01e1aa="">Login</div>
            </div>
        );
    }
}
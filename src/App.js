import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import HomeScreen from "./Screens/HomeScreen";


export default class App extends Component {
  render(){
      return (
        <Router>
          <div>
            <Switch>
              <Route exact path="/">
                <HomeScreen />
              </Route>
              
            </Switch>
          </div>
        </Router>
      );
  }
}


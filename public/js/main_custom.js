let svgContainer = document.querySelector('.bodymovinanim');
      if(svgContainer && svgContainer.length) {
        let animItem = bodymovin.loadAnimation({
          wrapper: svgContainer,
          animType: 'svg',
          loop: true,
          path: `https://sitethemedata.com/v3/static/front/js/Loader-v6.0.json?ver=117`
        });
      }
      
      function getWidth() {
        var doc = document,
            w = window;
        var docEl = (doc.compatMode && doc.compatMode === 'CSS1Compat') ?
            doc.documentElement : doc.body;
      
        var width = docEl.clientWidth;
        var height = docEl.clientHeight;
      
        if (w.innerWidth && width > w.innerWidth) {
            width = w.innerWidth;
            height = w.innerHeight;
        }
        return width
      }
      
      IS_MOBILE = getWidth() < 1280 ? true : false
      IS_TABLET = getWidth() >= 768 ? true : false
      IP_ADDRESS = localStorage.getItem("clientAddr");
      
      DOMAIN = "so777.in"
      LOGO_PATH = "https://sitethemedata.com/sitethemes/so777.in/front/logo.png"
      THEME = "dark"
      
      if(THEME == 'light'){
        if(DOMAIN == 'world777.com'){
          LOGO_PATH = `https://sitethemedata.com/v3/static/front/img/logo-blue.png?ver=117`;
        }
        document.documentElement.dataset.theme = 'light';
      }
      else if(THEME == 'blue'){
        if(DOMAIN == 'world777.com'){
          LOGO_PATH = `https://sitethemedata.com/v3/static/front/img/logo-blue.png?ver=117`;
        }
        document.documentElement.dataset.theme = 'blue';
      }
      else if(THEME == 'dark'){
        document.documentElement.dataset.theme = 'dark';
      }

      $("#yes_div, #no_div").owlCarousel({
        loop: !1,
        margin: 0,
        responsiveClass: !0,
        slideBy: 5,
        dots: !1,
        responsive: {
            0: {
                items: 4,
                nav: !0
            },
            320: {
                items: 4,
                nav: !0
            },
            360: {
                items: 5,
                nav: !0
            },
            411: {
                items: 6,
                nav: !0
            }
        }
    })
